# How to test

I tried to make system testing as easy as possible.
For that there is some boiler plate you have to set up for each test case:

```python
import unittest
from CLI import parsing_loop, make_register, Settings
from utils.TestIO import FakeInputGiver, FakeOutputReceiver


class TestCommandNameAndScenario(unittest.TestCase):

    def test_expected_behaviour_on_what_input(self):
        settings = Settings()
        in_ = FakeInputGiver.new("your cli", "input here", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
```

For a better idea of what failed when, I recommend to name your test class after the user invoked functionality
that you want to test.    
The name of the test function should reflect what the desired behaviour is given the in the test created
circumstances.   
You can give user input by simply adding them to the `FakeInputGiver`.    
Don't forget to use quit as the last parameter,
since the parsing loop will not exit otherwise and you have a infinite while loop.    
After the loops has finished execution, all the output will be saved inside the `FakeOutputReceiver`.

An real test case would look like this:

```python
...


class TestIgnoreCommandRemove(unittest.TestCase):

    def test_removing_from_the_list_removes_from_the_list(self):
        settings = Settings()
        settings.ignored_elements = ["this.file"]
        in_ = FakeInputGiver.new("ignore -r this.file", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")
```
