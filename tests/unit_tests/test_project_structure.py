import os
import unittest

from Library import project_structure
from Library.ospp import Searchable


def in_sublist_at(list_, pos: int, obj) -> bool:
    return True if True in [x[pos] == obj for x in list_] else False


class Structure(unittest.TestCase):
    def test_whole_structure(self):
        dummy_data = os.sep.join(__file__.split(os.sep)[:-2] + ["dummy_data", "discover_folder"])

        a = project_structure(dummy_data, [])

        # for e in a:
        #     print(e)

        self.assertTrue(in_sublist_at(a, 0, Searchable.PYTHON))
        self.assertEqual(
            f"pops{os.sep}tests{os.sep}dummy_data{os.sep}discover_folder{os.sep}demo.py",
            os.sep.join(a[0][1].split(os.sep)[-5:])
        )
        self.assertTrue(in_sublist_at(a, 0, Searchable.FOLDER))
        self.assertTrue(in_sublist_at(a[1][2], 0, Searchable.TEST))
        self.assertTrue(in_sublist_at(a, 0, Searchable.MODULE))
        self.assertTrue(in_sublist_at(a[2][2], 0, Searchable.INIT))


if __name__ == '__main__':
    unittest.main()
