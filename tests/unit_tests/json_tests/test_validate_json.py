"""
Testing implementation of json parsing logic
"""
import os
import re
import unittest

from utils.Json.exceptions import TypeMismatch, IncompleteSchemaDefinition, MissingAttribute, InvalidJsonFormat
from utils.Json.validate import validate_json


def settings_schema() -> dict:
    """
    :return: schema for the current version of settings
    """
    return {
        "project_location": str,
        "remote_repository": str,
        "remote_repository_type": str,
        "username": str,
        "password": str,
        "theme": str,
        "ignored_elements": {"type": list, "elements": str},
    }


def load_file(name) -> str:
    """
    loads the file to test on
    :param name: name of the file
    :return: the files content
    """
    path = os.sep.join(__file__.split(os.sep)[:-3] + ["dummy_data", "validate_json", name])
    with open(path) as f:
        content = f.read()
    return content


class TestValidate(unittest.TestCase):

    def test_valid_json_passes(self):
        content = load_file("valid_settings.json")
        validate_json(content, settings_schema())

    def test_json_with_type_mismatch_caught(self):
        content = load_file("type_mismatch.json")
        with self.assertRaises(TypeMismatch):
            validate_json(content, settings_schema())

    def test_json_with_more_defined_fields_than_in_schema_caught(self):
        content = load_file("additional_fields.json")
        with self.assertRaises(IncompleteSchemaDefinition):
            validate_json(content, settings_schema())

    def test_json_with_less_defined_fields_than_in_schema_caught(self):
        content = load_file("missing_attributes.json")
        with self.assertRaises(MissingAttribute):
            validate_json(content, settings_schema())

    def test_validate_works_on_rough_iter_definition(self):
        content = load_file("right_iter_def.json")
        validate_json(content, {"list": list})

    def test_validate_works_on_precise_iter_definitions(self):
        content = load_file("right_iter_def.json")
        validate_json(content, {"list": {"type": list, "elements": str}})

    def test_validate_catches_wrong_rough_iter_definition(self):
        content = load_file("wrong_iter_def.json")
        with self.assertRaises(TypeMismatch):
            validate_json(content, {"list": set})

    def test_validate_catches_wrong_precise_iter_definitions(self):
        content = load_file("wrong_iter_def.json")
        with self.assertRaises(TypeMismatch):
            validate_json(content, {"list": {"type": list, "elements": str}})

    def test_empty_files_are_not_json(self):
        content = load_file("empty.json")
        with self.assertRaises(InvalidJsonFormat):
            validate_json(content, settings_schema())

    def test_not_json_format_gets_detected_and_flagger(self):
        content = load_file("not_a_json.txt")
        with self.assertRaises(InvalidJsonFormat):
            validate_json(content, settings_schema())

    def test_passing_parsing_doesnt_qualify_as_json(self):
        content = load_file("just_a_list.json")
        with self.assertRaises(InvalidJsonFormat):
            validate_json(content, settings_schema())

    def test_parsing_with_list_of_valid_dicts(self):
        content = load_file("valid_composite_types_in_list.json")
        validate_json(content, {"list": {"type": list, "elements": {"one": int, "two": int}}})

    def test_parsing_with_list_of_invalid_dicts(self):
        content = load_file("invalid_composite_types_in_list.json")
        with self.assertRaises(TypeMismatch):
            validate_json(content, {"list": {"type": list, "elements": {"one": int, "two": str}}})

    def test_enum_types_work(self):
        content = load_file("enum_test.json")
        validate_json(content, {"value": (1, 2, 3)})

    def test_enum_type_mismatch_are_caught(self):
        content = load_file("enum_test.json")
        with self.assertRaises(TypeMismatch):
            validate_json(content, {"value": (1, 2)})

    def test_regex_checks_work(self):
        content = load_file("regex_test.json")
        validate_json(content, {"value": re.compile("#[0-9a-fA-F]{6}|#[0-9a-fA-F]{3}")})

    def test_regex_type_mismatch_are_caught(self):
        content = load_file("regex_test.json")
        with self.assertRaises(TypeMismatch):
            validate_json(content, {"value": re.compile("#[0-9a-fA-F]{6}")})


if __name__ == '__main__':
    unittest.main()
