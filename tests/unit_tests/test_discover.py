import os
import unittest


class Discover(unittest.TestCase):

    def test_discover_folder(self):
        from Library.ospp import discover, Searchable

        root_ = os.sep.join(__file__.split(os.sep)[:-2]) + f"{os.sep}dummy_data{os.sep}discover_folder"

        self.assertEqual(['demo_folder', 'demo_module', 'sub module'], discover(root_, Searchable.FOLDER))

    def test_discover_py_files(self):
        from Library.ospp import discover, Searchable

        root_ = os.sep.join(__file__.split(os.sep)[:-2]) + f"{os.sep}dummy_data{os.sep}discover_folder"

        self.assertEqual(['demo.py', 'module_demo.py'], discover(root_, Searchable.PYTHON))

    def test_discover_test_files(self):
        from Library.ospp import discover, Searchable

        root_ = os.sep.join(__file__.split(os.sep)[:-2]) + f"{os.sep}dummy_data{os.sep}discover_folder"

        self.assertEqual(['test_demo.py'], discover(root_, Searchable.TEST))

    def test_discover_inti_files(self):
        from Library.ospp import discover, Searchable

        root_ = os.sep.join(__file__.split(os.sep)[:-2]) + f"{os.sep}dummy_data{os.sep}discover_folder"

        self.assertEqual({'demo_module.__init__.py', 'sub module.__init__.py'}, set(discover(root_, Searchable.INIT)))

    def test_discover_modules(self):
        from Library.ospp import discover, Searchable

        root_ = os.sep.join(__file__.split(os.sep)[:-2]) + f"{os.sep}dummy_data{os.sep}discover_folder"

        self.assertEqual(['demo_module', 'sub module'], discover(root_, Searchable.MODULE))

    def test_discover_ignored_file(self):
        from Library.ospp import discover, Searchable

        root_ = os.sep.join(__file__.split(os.sep)[:-2]) + f"{os.sep}dummy_data{os.sep}discover_folder"

        self.assertEqual(['demo.py'], discover(root_, Searchable.PYTHON, ['module_demo.py']))

    def test_discover_ignored_folder(self):
        from Library.ospp import discover, Searchable

        root_ = os.sep.join(__file__.split(os.sep)[:-2]) + f"{os.sep}dummy_data{os.sep}discover_folder"

        self.assertEqual(['demo_folder'], discover(root_, Searchable.FOLDER, ['demo_module']))


if __name__ == '__main__':
    unittest.main()
