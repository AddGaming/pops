"""
Tests that changing the settings works
"""
import os
import unittest

from CLI import parsing_loop, make_register, Settings
from utils.TestIO import FakeInputGiver, FakeOutputReceiver


# noinspection DuplicatedCode
class TestListing(unittest.TestCase):

    def test_all_settings_fields_are_given_except_ignored(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("project_location" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("remote_repository" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("remote_repository_type" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("username" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("password" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("theme" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("ignored_elements" not in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_values_are_up_to_date_in_output(self):
        settings = Settings()
        settings.remote_repository_type = "GitHub"
        in_ = FakeInputGiver.new("settings", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("GitHub" in "\n".join(out_.outputs), f"{out_.outputs=}")
        settings.remote_repository_type = "GitLab"
        in_ = FakeInputGiver.new("settings", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("GitHub" not in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("GitLab" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_everything_has_its_own_line(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 7, f"{out_.outputs=}")


# noinspection DuplicatedCode
class TestSpecificArgument(unittest.TestCase):

    def test_calling_with_value_gives_information_on_that_value_alone(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings username", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("project_location" not in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("username:" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_trailing_whitespace_is_ignored(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings username         ", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("username:" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_whitespace_in_the_middle_is_ignored(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings      username", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("username:" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_inquiry_with_non_existing_field_gives_error(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings user", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("user" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("doesn't exists" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_asking_for_ignored_elements_this_way_gives_error(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings ignored_elements", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(
            "Please use the 'ignore' keyword to set/get ignored elements" in "\n".join(out_.outputs),
            f"{out_.outputs=}"
        )


# noinspection DuplicatedCode
class TestChanging(unittest.TestCase):

    def test_changing_works(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings username tester", "settings username", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("username:" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("tester" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_arguments_keep_case(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings username TesteR", "settings username", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("username:" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("TesteR" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_changing_non_existing_setting_throws_error(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings test tester", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("test" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("doesn't exists" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_changing_project_location_to_non_valid_path_throws_error(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings project_location tester", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("The path you entered is not valid" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_changing_project_location_to_valid_but_non_existing_path_throws_error(self):
        settings = Settings()
        in_ = FakeInputGiver.new(
            f"settings project_location {os.sep.join(__file__.split(os.sep)[:-1] + ['testing'])}",
            "quit"
        )
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("The path you entered is not valid" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_changing_project_location_to_valid_path(self):
        settings = Settings()
        in_ = FakeInputGiver.new(
            f"settings project_location {os.sep.join(__file__.split(os.sep)[:-1])}",
            "settings project_location",
            "quit"
        )
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(f"{os.sep.join(__file__.split(os.sep)[:-1])}" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_changing_project_location_to_valid_path_with_spaces_in_name(self):
        settings = Settings()
        test_folder = __file__.split(os.sep)[:-3]
        rel_folder_path = ['dummy_data', 'discover_folder', 'demo_module', 'sub module']
        path = os.sep.join(test_folder + rel_folder_path)
        in_ = FakeInputGiver.new(
            f"settings project_location \"{path}\"",
            "settings project_location",
            "quit"
        )
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(f"{path}" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_whitespace_gets_ignored(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings       username      tester", "settings username", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("username:" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("tester" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("   tester" not in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_whitespace_gets_trimmed(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings username tester       ", "settings username", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("username:" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("tester" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("tester   " not in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_quoted_parameters_work(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings username 'tester'", "settings username", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("username:" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("tester" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("'tester'" not in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_names_with_spaces_work(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings username 'name with spaces'", "settings username", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("username:" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("name with spaces" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_accessing_ignored_elements_this_way_results_in_error(self):
        settings = Settings()
        in_ = FakeInputGiver.new("settings ignored_elements", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(
            "Please use the 'ignore' keyword to set/get ignored elements" in "\n".join(out_.outputs),
            f"{out_.outputs=}"
        )

    def test_remote_repository_type_gets_inferred_by_url(self):
        test_cases = [
            ("https://github.com/BigDataAnalyticsGroup/python", "GitHub"),
            ("https://gitlab.com/AddGaming/pops", "GitLab"),
            ("git@github.com:BigDataAnalyticsGroup/python.git", "GitHub"),
            ("git@gitlab.com:AddGaming/pops.git", "GitLab"),
        ]
        settings = Settings()
        for test_case in test_cases:
            in_ = FakeInputGiver.new(
                f"settings remote_repository '{test_case[0]}'",
                "settings remote_repository_type",
                "quit"
            )
            out_ = FakeOutputReceiver.new()
            parsing_loop(make_register(settings), in_, out_)
            self.assertTrue("remote_repository_type:" in "\n".join(out_.outputs), f"{out_.outputs=}")
            self.assertTrue(test_case[1] in "\n".join(out_.outputs), f"{out_.outputs=}\nfor test case {test_case}")

    def test_remote_repository_type_doesnt_get_inferred_if_indecisive(self):
        settings = Settings()
        in_ = FakeInputGiver.new(
            "settings remote_repository \"github@gitlab.com:AddGaming/pops.git\"",
            "settings remote_repository_type",
            "quit"
        )
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("remote_repository_type:" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("GitHub" not in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("GitLab" not in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_remote_repository_type_doesnt_get_inferred_if_unknown(self):
        settings = Settings()
        in_ = FakeInputGiver.new(
            "settings remote_repository \"git@git.com:AddGaming/pops.git\"",
            "settings remote_repository_type",
            "quit"
        )
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("remote_repository_type:" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("GitHub" not in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("GitLab" not in "\n".join(out_.outputs), f"{out_.outputs=}")


# noinspection DuplicatedCode
class TestReset(unittest.TestCase):

    def test_resetting_works(self):
        settings = Settings()
        settings.remote_repository = "git@gitlab.com:AddGaming/pops.git"
        settings.remote_repository_type = "GitLab"
        settings.username = "YesMan"
        in_ = FakeInputGiver.new(
            "settings",
            "quit"
        )
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("GitLab" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("git@gitlab.com:AddGaming/pops.git" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("YesMan" in "\n".join(out_.outputs), f"{out_.outputs=}")
        # resetting
        in_ = FakeInputGiver.new(
            "settings reset",
            "settings",
            "quit"
        )
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("reset" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("GitLab" not in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("git@gitlab.com:AddGaming/pops.git" not in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("YesMan" not in "\n".join(out_.outputs), f"{out_.outputs=}")


if __name__ == "__main__":
    unittest.main()
