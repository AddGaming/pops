"""
tests the help command
"""
import unittest

from CLI import parsing_loop, make_register
from utils.TestIO import FakeInputGiver, FakeOutputReceiver


class TestHelpCommand(unittest.TestCase):

    def test_empty_invocation_returns_all_help(self):
        in_ = FakeInputGiver.new("help", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(), in_, out_)
        self.assertTrue("help" in out_.outputs[0])
        for key in make_register():
            self.assertTrue(key in out_.outputs[0])

    def test_multiple_commands_with_the_same_function_are_grouped(self):
        in_ = FakeInputGiver.new("help", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(), in_, out_)
        self.assertTrue("cls | clear" in out_.outputs[0])

    def test_partial_function_application_have_overwritten_docstring(self):
        in_ = FakeInputGiver.new("help", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(), in_, out_)
        self.assertTrue("partial(func, *args, **keywords)" not in out_.outputs[0], f"{out_.outputs}")

    def test_invocation_with_parameter(self):
        in_ = FakeInputGiver.new("help quit", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(), in_, out_)
        self.assertTrue("shuts down the cli safely" in out_.outputs[0])
        self.assertTrue(":return: False" not in out_.outputs[0])

    def test_invocation_with_parameter_that_doesnt_exists_throws_error(self):
        in_ = FakeInputGiver.new("help majesty", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(), in_, out_)
        self.assertTrue("majesty" in out_.outputs[0])
        self.assertTrue("does not exist" in out_.outputs[0])


if __name__ == '__main__':
    unittest.main()
