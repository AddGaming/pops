"""
tests the ignore command from the CLI
"""
import unittest

from CLI import parsing_loop, make_register, Settings
from utils.TestIO import FakeInputGiver, FakeOutputReceiver


# noinspection DuplicatedCode
class TestIgnoreCommandWOArgs(unittest.TestCase):

    def test_invocation_on_new_settings_is_empty(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_folder_section_appears_if_at_leased_one_folger_is_ignored(self):
        settings = Settings()
        settings.ignored_elements = ["venv"]
        in_ = FakeInputGiver.new("ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("Folders" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("venv" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_file_section_appears_if_at_leased_one_file_is_ignored(self):
        settings = Settings()
        settings.ignored_elements = [".gitignore"]
        in_ = FakeInputGiver.new("ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("Files" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue(".gitignore" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_folder_and_file_section_appear_if_at_leased_one_folger_and_one_file_is_ignored(self):
        settings = Settings()
        settings.ignored_elements = [".gitignore", "venv"]
        in_ = FakeInputGiver.new("ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("Folders" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("venv" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("Files" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue(".gitignore" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_list_in_each_section_matches_the_amount_of_ignored_elements(self):
        settings = Settings()
        settings.ignored_elements = [".gitignore"]
        in_ = FakeInputGiver.new("ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        # this may look stupid, but one output can have multiple newlines
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 3)

        settings.ignored_elements = [".gitignore", "main.py"]
        in_ = FakeInputGiver.new("ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 4)


# noinspection DuplicatedCode
class TestIgnoreCommandAdding(unittest.TestCase):

    def test_adding_to_the_list_adds_to_the_list(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore this.file", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 3, f"{out_.outputs=}")
        self.assertTrue("this.file" in "".join(out_.outputs))

    def test_adding_to_the_list_adds_to_the_list_with_trailing_whitespace_cut(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore this.file       ", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 3, f"{out_.outputs=}")
        self.assertFalse(out_.outputs[1].endswith(" "))

    def test_repeated_adding_to_the_list_adds_everything(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore this.file", "ignore another.file", "ignore test_directory", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 6, f"{out_.outputs=}")
        self.assertTrue("this.file" in "".join(out_.outputs))
        self.assertTrue("another.file" in "".join(out_.outputs))
        self.assertTrue("test_directory" in "".join(out_.outputs))

    def test_adding_noting_adds_noting(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore   ", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len(out_.outputs) == 2, f"{out_.outputs=}")
        self.assertTrue("no ignored elements" in "".join(out_.outputs))

    def test_adding_files_with_spaces_in_them(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore \"this is a valid.file\"", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 3, f"{out_.outputs=}")

    def test_adding_files_with_spaces_in_them_with_trailing_white_space_cut(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore \"this is a valid.file\"   ", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 3, f"{out_.outputs=}")
        self.assertFalse(out_.outputs[1].endswith(" "))

    def test_adding_multiple_elements_at_once(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore this.file another.file test_directory", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 6, f"{out_.outputs=}")

    def test_adding_multiple_files_with_spaces_in_them(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore \"t h i s.file\" \"ano t h e r.file\"  \"test directory\"", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        output = "\n".join(out_.outputs)
        self.assertTrue(len(output.split("\n")) == 6, f"{output=}")

    def test_missing_quotation_mark_inserted_in_the_end(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore \"this is a valid.file", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 3, f"{out_.outputs=}")
        self.assertTrue('this is a valid.file' in "".join(out_.outputs))

    def test_american_quotes_also_work(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore 'this is a valid.file'", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 3, f"{out_.outputs=}")
        self.assertTrue('this is a valid.file' in "".join(out_.outputs))

    def test_superfluous_white_space_gets_ignored(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore    this.file", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 3, f"{out_.outputs=}")
        self.assertTrue("this.file" in "".join(out_.outputs))

    def test_adding_with_superfluous_marks_ignores_them(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore 'this.file'", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue(len("\n".join(out_.outputs).split("\n")) == 3, f"{out_.outputs=}")
        self.assertTrue("this.file" in "".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("'this.file'" not in "".join(out_.outputs), f"{out_.outputs=}")


# noinspection DuplicatedCode
class TestIgnoreCommandRemove(unittest.TestCase):

    def test_removing_from_the_list_removes_from_the_list(self):
        settings = Settings()
        settings.ignored_elements = ["this.file"]
        in_ = FakeInputGiver.new("ignore -r this.file", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_removing_from_the_list_with_written_out_flag_removes_from_the_list(self):
        settings = Settings()
        settings.ignored_elements = ["this.file"]
        in_ = FakeInputGiver.new("ignore -remove this.file", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_removing_works_with_trailing_whitespaces_whitespace_cut(self):
        settings = Settings()
        settings.ignored_elements = ["this.file"]
        in_ = FakeInputGiver.new("ignore -r this.file       ", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_superfluous_white_space_gets_ignored(self):
        settings = Settings()
        settings.ignored_elements = ["this.file"]
        in_ = FakeInputGiver.new("ignore         -r          this.file", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_repeated_removing_from_the_list_removes_everything(self):
        settings = Settings()
        settings.ignored_elements = ["this.file", "another.file", "test_directory"]
        in_ = FakeInputGiver.new(
            "ignore -r this.file",
            "ignore -r another.file",
            "ignore -r test_directory",
            "ignore",
            "quit"
        )
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_errors_while_chain_removing_are_given_but_dont_interrupt_the_removal_of_other_items(self):
        settings = Settings()
        settings.ignored_elements = ["this.file", "another.file", "test_directory"]
        in_ = FakeInputGiver.new(
            "ignore -r this.file another.f test_directory",
            "ignore",
            "quit"
        )
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("another.f is not ignored" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("another.file" in "\n".join(out_.outputs[1:-1]), f"{out_.outputs=}")

    def test_removing_noting_removes_noting(self):
        settings = Settings()
        settings.ignored_elements = ["this.file", "another.file", "test_directory"]
        in_ = FakeInputGiver.new("ignore -r  ", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("test_directory" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("another.file" in "\n".join(out_.outputs), f"{out_.outputs=}")
        self.assertTrue("this.file" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_removing_files_with_spaces_in_them(self):
        settings = Settings()
        settings.ignored_elements = ["this is a valid.file"]
        in_ = FakeInputGiver.new("ignore -r \"this is a valid.file\"", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_removing_files_with_spaces_in_them_with_trailing_white_space_cut(self):
        settings = Settings()
        settings.ignored_elements = ["this is a valid.file"]
        in_ = FakeInputGiver.new("ignore -r \"this is a valid.file\"   ", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_removing_multiple_elements_at_once(self):
        settings = Settings()
        settings.ignored_elements = ["this.file", "another.file", "test_directory"]
        in_ = FakeInputGiver.new("ignore -r this.file another.file test_directory", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_removing_multiple_files_with_spaces_in_them(self):
        settings = Settings()
        settings.ignored_elements = ["t h i s.file", "ano t h e r.file", "test directory"]
        in_ = FakeInputGiver.new("ignore -r \"t h i s.file\" \"ano t h e r.file\"  \"test directory\"", "ignore",
                                 "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_missing_quotation_mark_inserted_in_the_end(self):
        settings = Settings()
        settings.ignored_elements = ["this is a valid.file"]
        in_ = FakeInputGiver.new("ignore -r \"this is a valid.file", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_american_quotes_also_work(self):
        settings = Settings()
        settings.ignored_elements = ["this is a valid.file"]
        in_ = FakeInputGiver.new("ignore -r 'this is a valid.file'", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("no ignored elements" in "\n".join(out_.outputs), f"{out_.outputs=}")

    def test_removing_something_that_is_not_on_the_list_gives_error(self):
        settings = Settings()
        in_ = FakeInputGiver.new("ignore -r 'another.f'", "ignore", "quit")
        out_ = FakeOutputReceiver.new()
        parsing_loop(make_register(settings), in_, out_)
        self.assertTrue("another.f is not ignored" in "\n".join(out_.outputs), f"{out_.outputs=}")


if __name__ == "__main__":
    unittest.main()
