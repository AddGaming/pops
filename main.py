"""
CLI for Pops
"""
import os

from CLI import parsing_loop, make_register

if __name__ == "__main__":
    os.chdir(os.sep.join(__file__.split(os.sep)[:-1]))

    parsing_loop(make_register(), input, print)
