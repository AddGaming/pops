"""
The functions that the module should expose directly
"""
from Library.formatting import project_to_mermaid, mermaid_to_file, FileFormats, structure_to_documentation, \
    documentation_to_file
from Library.structure_analysis import project_structure


def graph(
    path: str, style: str, ignored: [str] = None, direction: str = "TB", details: int = 1
) -> str:
    """
    creates a graphical representation of the internal project structure.
    :param path: the path to the project
    :param details: the amount of details that should be included 0=low...2=high (default=1)
    :param ignored: a list of ignored directory names (default = ["venv", "__pycache__"])
    :param style: the style/theme used for the graph (default = "default")
    :param direction: the direction of the flowchart (TB|LR) (default = "TB")
    :return: str - representation
    """
    mermaid = project_to_mermaid(path, ignored, style, direction, details)
    return mermaid_to_file(mermaid, FileFormats.MD)


def document(path: str, ignored: [str] = None):
    """
    TODO
    :param path:
    :param ignored:
    :return:
    """
    if not ignored:
        ignored = []
    structure = project_structure(path, ignored)
    documentation = structure_to_documentation(structure)
    string = documentation_to_file(documentation, FileFormats.MD)
    return string
