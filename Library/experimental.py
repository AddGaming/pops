import ast

doc_map = {}


def write_doc_to_md(map_: dict = None):
    if map_ is None:
        map_ = doc_map
    fstring = ""
    sorted_modules = [key for key in map_]
    sorted_modules.sort()
    for module_name in sorted_modules:
        fstring += f"<details>\n<summary>\n\n# {module_name}\n</summary>\n\n"
        fstring += f"{map_[module_name]['doc']}\n"
        sorted_files = [file for file in map_[module_name]["files"]]
        sorted_files.sort()
        for file_name in sorted_files:
            fstring += f"<details>\n<summary>\n\n## {file_name}\n</summary>\n\n"
            fstring += f"{map_[module_name]['files'][file_name]['doc']}\n"
            sorted_functions = [name for name in map_[module_name]["files"][file_name]["functions"]]
            sorted_functions.sort()
            for name in sorted_functions:
                fstring += f"### {name}\n\n{map_[module_name]['files'][file_name]['functions'][name]}\n\n"

            fstring += f"</details>\n"

        fstring += f"</details>\n\n"

    with open("all_in_one_doc.md", "w") as target:
        target.writelines(fstring)
    return fstring


def add_to_doc_map(
    module: str, module_doc: str, file: str, file_doc: str, function_name: str, doc: str, map_: dict = None
):
    # doc_map[module] = module
    # doc_map[module][files] = files
    # doc_map[module][doc] = doc
    # doc_map[module][files][file] = file
    # doc_map[module][files][file][functions] = functions
    # doc_map[module][files][file][doc] = doc
    # doc_map[module][files][file][functions][name] = doc
    if map_ is None:
        map_ = doc_map
    map_[module]["doc"] = module_doc
    map_[module]["files"][file]["doc"] = file_doc
    map_[module]["files"][file]["functions"][function_name] = doc
    return map_


def ast_analysis(
    file: str = "E:\\uni5\\med-project\\git\\chat-bot-storyteller\\database\\data_tools\\app_data_interface.py",
    first: bool = False
):
    with open(file) as f:
        code = f.read()
    global doc_map
    doc_map = {}
    node = ast.parse(code)

    # print(root_node._fields)
    for i, thing in enumerate(node.body):
        print("=" * 40)
        print(thing)
        match thing.__class__:
            case ast.Expr:  # <- if first element is expr then this is doc string!
                print(thing._fields)
                print(thing.value)
                print(thing.value._fields)
                print(thing.value.value)
                if first:
                    first = False
                """case ast.Import:
                    print(thing._fields)
                    print(thing.names)
                    for name in thing.names:
                        print(name._fields)
                        print(name.name)
                        print(name.asname)  # file internal stuff. can be ignored
                case ast.ImportFrom:
                    print(thing._fields)
                    print(thing.module)
                    print(thing.names)
                    for name in thing.names:
                        print(name._fields)
                        print(name.name)
                        print(name.asname)  # file internal stuff. can be ignored
                    print(thing.level)"""
            case ast.ClassDef:
                print(thing._fields)
                print(thing.name)
                print(thing.bases)
                print(thing.keywords)
                print(thing.body)
                print(thing.decorator_list)
            case _:
                print("not recognized yet")

    return doc_map


class ASTVisitor(ast.NodeVisitor):
    """
    Visits all the sub-nodes and start-root_node that are of interest
    Currently unsure if this is usefully/ how to use it
    """

    def visit_ClassDef(self, node: ast.AST):
        print(node)
        self.generic_visit(node)

    def visit_FunctionDef(self, node: ast.AST):
        print(node)
        self.generic_visit(node)

    def visit_AsyncFunctionDef(self, node: ast.AST):
        print(node)
        self.generic_visit(node)
