import os
import re

from utils.Json import validate_json, MissingAttribute, TypeMismatch, \
    IncompleteSchemaDefinition, InvalidJsonFormat


class MermaidStyles:
    """
    Class for styling Mermaid graphs
    """
    styles: dict = None  # the registered styles

    def __init__(self, styles):
        self.styles = styles

    # hard-coding the schema since the structure is important for functionality of the rest of the code
    @staticmethod
    def _schema() -> dict:
        """
        The validation schema
        :return: Json-Validation-Schema
        """
        return {
            "styles": {
                "type": list,
                "elements": {
                    "name": str,
                    "def_type": ("classDef", "linkStyle"),
                    "fill": re.compile("#[0-9a-fA-F]{6}|#[0-9a-fA-F]{3}"),
                    "stroke": str,
                    "stroke-width": int,
                }
            },
        }

    @classmethod
    def load_from_file(cls):
        """
        loads the settings from the save file
        :return: Settings
        """
        path = os.sep.join(__file__.split(os.sep)[:-1])
        theme_files = os.listdir(f"{path}{os.sep}mermaid_themes")
        theme_files.remove("__init__.py")
        styles = {}

        for theme in theme_files:
            with open(f'{path}{os.sep}mermaid_themes{os.sep}{theme}', "r") as f:
                content = f.read()
            try:
                validate_json(content, cls._schema())
                styles[theme.split(".")[0]] = content
            except (MissingAttribute, TypeMismatch, IncompleteSchemaDefinition, InvalidJsonFormat) as e:
                print(str(e))
                print(f"{theme} is invalid\n\t-> canceling parsing...")

        return cls(styles=styles)

    def list_styles(self) -> [str]:
        """
        :return: [str] - list of the registered styles
        """
        return self.styles.keys()
