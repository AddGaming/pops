"""
functions for creating the required mark-down files
"""
import csv


def md_table(array, name="default.md"):
    """
    Writes a markdown file
    """
    output = []
    temp = "|"
    temp2 = temp
    # header
    for elem in array:
        for key in elem:
            temp += f" {key} |"
            temp2 += f" {len(key) * '-'} |"
        break
    output.append(temp + "\n")
    output.append(temp2 + "\n")
    # body
    for elem in array:
        temp = "|"
        for key in elem:
            temp += f" {elem[key]} |"
        output.append(temp + "\n")
    # write
    with open(name, "w") as target:
        target.writelines(output)


def min_hour_guy(time_list):
    """
    finds the person that has the least amount of time planned for
    """
    min_name = ""
    min_value = 999999999
    for name, data in time_list.items():
        if name == "Simon JÃ¤ckel":  # aski sucks
            continue
        if data["Must Have"]["Time Estimate"] + data["May Have"]["Time Estimate"] < min_value:
            min_name = name
            min_value = data["Must Have"]["Time Estimate"] + data["May Have"]["Time Estimate"]

    return min_name


# noinspection DuplicatedCode
def generate_timetable_md(loc):
    """
    writes an analysis of the time spend on the project according to the documentation requirements from university
    """
    with open(loc) as file:
        # format_table addgaming-chat-bot-storryteller_issues_2021-11-14.csv
        reader: csv.DictReader = csv.DictReader(file)
        table = []
        for row in reader:
            temp = {}
            for key in row:
                x = row[key].strip()
                x = x.replace("TheFreschOne", "Maximilian Hohlweck")
                x = x.replace("lennart", "Lennart Asal")
                x = x.replace("Add Gaming", "Raphael Diener")
                x = x.replace("\n", "")
                temp[key] = x
            table.append(temp)

        # count time spent
        task_lists = {}
        for row in table:
            if row["State"] == "Closed":
                for i in range(1, 4):
                    if "Difficulty::" + str(i) in row["Labels"].split(","):
                        try:
                            task_lists["Difficulty::" + str(i)]["Count"] += 1
                            task_lists["Difficulty::" + str(i)]["Time"] += int(row["Time Spent"])
                        except KeyError:
                            task_lists["Difficulty::" + str(i)] = {
                                "Count": 1,
                                "Time": int(row["Time Spent"])
                            }

                # additional info, prob not required or used
                for person in row["Assignee"].split(","):
                    try:
                        task_lists[person].append(row["Title"])
                    except KeyError:
                        task_lists[person] = [row["Title"]]

        for i in range(1, 4):
            try:
                print(f"Difficulty::{i} = {task_lists['Difficulty::' + str(i)]} => "
                      f"{task_lists['Difficulty::' + str(i)]['Time'] / task_lists['Difficulty::' + str(i)]['Count'] / 3600}")
            except KeyError:
                print(f"Difficulty::{i} = None => {i * i * 2}")
                task_lists['Difficulty::' + str(i)] = {"Time": i * i * 2 * 3600, "Count": 1}

        # adjusting time estimations by projection
        for row in table:
            if row["State"] == "Open":
                for i in range(1, 4):
                    if "Difficulty::" + str(i) in row["Labels"].split(","):
                        row["Time Estimate"] = \
                            task_lists["Difficulty::" + str(i)]["Time"] / task_lists["Difficulty::" + str(i)]["Count"]

        md_table(table, name="table.md")

        # count issue lables
        lable_count = {}
        for row in table:
            for lable in row["Labels"].split(","):
                if lable == "":
                    lable = "unlabled"
                try:
                    lable_count[lable] += 1
                except KeyError:
                    lable_count[lable] = 1

        md_table([lable_count], name="lable_count.md")

        # create "board-like" issue table

        for row in table:
            for lable in row["Labels"].split(","):

                temp = {}

                for key in row:
                    if key in ["Issue ID", "Title", "Description", "Milestone", "State", "Time Estimate", "Time Spent",
                               "Closed At (UTC)"]:
                        temp[key] = row[key]
                temp["Time Spent"] = f'{int(row["Time Spent"]) // 3600}h ' \
                                     f'{int(row["Time Spent"]) % 3600 // 60}m'
                temp["Time Estimate"] = f'{int(row["Time Estimate"]) // 3600}h ' \
                                        f'{int(row["Time Estimate"]) % 3600 // 60}m'
                temp["Main Responsible"] = row["Assignee"].split(",")[0]
                temp["Assignee"] = row["Assignee"]

        # count time
        time_list = {}
        for index, row in enumerate(table):
            if not row["Assignee"]:  # nobody has claimed yet
                new_row = row
                new_row["Assignee"] = min_hour_guy(time_list)
                table[index] = new_row
            persons = row["Assignee"].split(",")
            for i in range(len(persons)):
                persons[i] = persons[i].strip()
            for person in persons:
                try:
                    time_list[person][row["Milestone"]]["Time Estimate"] += int(row["Time Estimate"]) / len(persons)
                    time_list[person][row["Milestone"]]["Time Spent"] += int(row["Time Spent"]) / len(persons)
                except KeyError:
                    time_list[person] = {
                        "Must Have": {
                            "Time Estimate": 0,
                            "Time Spent": 0
                        },
                        "May Have": {
                            "Time Estimate": 0,
                            "Time Spent": 0
                        }
                    }
                    time_list[person][row["Milestone"]]["Time Estimate"] = int(row["Time Estimate"]) / len(persons)
                    time_list[person][row["Milestone"]]["Time Spent"] = int(row["Time Spent"]) / len(persons)

        # create overview table
        overview_table = []
        for row in table:
            temp = {}
            for key in row:
                if key in ["Issue ID", "Title",
                           "Description", "Milestone", "State", "Time Estimate", "Time Spent", "Closed At (UTC)"]:
                    temp[key] = row[key]

            temp["Time Spent"] = f"{int(temp['Time Spent']) // 3600}h {int(temp['Time Spent']) % 3600 // 60}m"
            temp["Time Estimate"] = f"{int(temp['Time Estimate']) // 3600}h {int(temp['Time Estimate']) % 3600 // 60}m"
            temp["Main Responsible"] = row["Assignee"].split(",")[0]
            temp["Assignee"] = row["Assignee"]
            overview_table.append(temp)

        md_table(overview_table, name="ov_table.md")

        # format time to table
        time_table = []
        for person in time_list:
            temp = {"Name": person,
                    "Time Estimate Must-have [in h]": f'{int(time_list[person]["Must Have"]["Time Estimate"]) // 3600}h '
                                                      f'{int(time_list[person]["Must Have"]["Time Estimate"]) % 3600 // 60}m',
                    "Time Estimate, Must + May-have[in h]": f'{int((time_list[person]["Must Have"]["Time Estimate"] + time_list[person]["May Have"]["Time Estimate"])) // 3600}h '
                                                            f'{int((time_list[person]["Must Have"]["Time Estimate"] + time_list[person]["May Have"]["Time Estimate"])) % 3600 // 60}m',
                    "Time Spent, Must + May-have [in h]": f'{int((time_list[person]["Must Have"]["Time Spent"] + time_list[person]["May Have"]["Time Spent"])) // 3600}h '
                                                          f'{int((time_list[person]["Must Have"]["Time Spent"] + time_list[person]["May Have"]["Time Spent"])) % 3600 // 60}m'}
            time_table.append(temp)

        md_table(time_table, name="time_table.md")
