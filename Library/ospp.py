"""
Operating System module enhanced version (os++)
"""
import enum
import glob
import os


class Searchable(enum.Enum):
    PYTHON = "python"
    MODULE = "module"
    FOLDER = "folder"
    TEST = "test"
    INIT = "init"


def find_file(file_name: str, search_root: str = "") -> str:
    """
    searches for the full path of the file.
    :raise RuntimeError: if no file can be found
    :param search_root: str - the location to search in (default = cwd)
    :param file_name: str - the name of the file
    :return: str - the absolute path of the file
    """

    if not search_root:
        search_root = os.getcwd()

    if file_name in discover(search_root, Searchable.MODULE):
        files = glob.glob(f"{search_root}{os.sep}**{os.sep}{file_name}{os.sep}__init__.py", recursive=True)
    else:
        if not file_name.endswith(".py"):
            file_name += ".py"
        files = glob.glob(f"{search_root}{os.sep}**{os.sep}{file_name}", recursive=True)

    if files:
        return files[0]
    else:

        raise RuntimeError(f"No files with the name {file_name} could be found in {search_root}")


def discover(root_: str, type_: Searchable, ignored: [str] = None) -> [str]:
    """
    Searches all content in given directory for elements of the desired type
    :param root_: str - the folder from which to start
    :param type_: Searchable - what to search for
    :param ignored: [str] - list of elements to ignore
    :return: [str] - list of elements with fitting type
    :raise NotImplementedError: if the provided Searchable is not yet searchable
    """

    result = []
    if not ignored:  # to avoid list sharing
        ignored = []

    for data_container in os.listdir(root_):
        if data_container.startswith(".") or data_container in ignored:
            continue
        if type_.value == "folder":
            if os.path.isdir(f"{root_}{os.sep}{data_container}"):
                result.append(data_container)
                result += discover(f"{root_}{os.sep}{data_container}", type_, ignored)

        elif type_.value == "module":
            if os.path.isdir(f"{root_}{os.sep}{data_container}"):
                if os.path.exists(f"{root_}{os.sep}{data_container}{os.sep}__init__.py"):
                    result.append(data_container)
                result += discover(f"{root_}{os.sep}{data_container}", type_, ignored)

        elif type_.value == "python":
            # ignoring __init__s for the moment
            if data_container.endswith(".py") \
                    and not data_container.startswith("__") \
                    and data_container.find("test") == -1:
                result.append(data_container)
            if os.path.isdir(f"{root_}{os.sep}{data_container}"):
                result += discover(f"{root_}{os.sep}{data_container}", type_, ignored)

        elif type_.value == "test":
            if data_container.endswith(".py") and data_container.find("test") != -1:
                result.append(data_container)
            if os.path.isdir(f"{root_}{os.sep}{data_container}"):
                result += discover(f"{root_}{os.sep}{data_container}", type_, ignored)

        elif type_.value == "init":
            if data_container.endswith(".py") and data_container.find("init") != -1:
                result.append(f"{root_.split(os.sep)[-1]}.{data_container}")
            if os.path.isdir(f"{root_}{os.sep}{data_container}"):
                result += discover(f"{root_}{os.sep}{data_container}", type_, ignored)

        else:
            raise NotImplementedError
    return result
