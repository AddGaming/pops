import ast
import enum
import os


# TODO: add support for format choice once class repr is implemented
class DiagramFormats(enum.Enum):
    """
    Supported diagram formats can can be processed
    """
    MERMAID = "mermaid flow"


# TODO: add support for format choice once more reprs are implemented
class FileFormats(enum.Enum):
    """
    Supported file formats content can be saved to
    """
    MD = "md"
    SVG = "svg"
    HTML = "html"
    PNG = "png"


def mermaid_to_file(mermaid: str, format_: FileFormats) -> str:
    """
    converts mermaid diagram string into embeddable format
    :param mermaid: the pure mermaid string
    :param format_: the format which to convert to
    :return: the formatted mermaid str ready for inclusion
    """
    if format_ == FileFormats.MD:
        return f"```mermaid\n{mermaid}\n```"
    elif format_ == FileFormats.HTML:
        return f"<div class='mermaid'>\n{mermaid}\n</div>"
    else:
        raise NotImplementedError


def documentation_to_file(doc: str, format_: FileFormats) -> str:
    result = []
    # [('root.CLI.CliFunctions.py', '\nWrapper around Library functions\n'),
    if format_ == FileFormats.MD:
        for doc_tup in doc:
            for i, parent in enumerate(doc_tup[0].split("|")[1:]):  # starting @ 1 to ignore project root
                string = f"{'#' * i} {parent}:\n"
                # pos = result.index(string)
                if string not in result:
                    result.append(string)
                else:
                    pass  # testing passing for the moment. probably needs further action

            result.append(doc_tup[1])

    else:
        raise NotImplementedError

    return "".join(result)


def project_to_mermaid(
        project_root: str = None, ignored: [str] = None, style: str = None, direction: str = "TB",
        details: int = 1) -> str:
    """
    converts a python project into a mermaid graph
    :param details: the amount of details that should be included 0=low...2=high (default=1)
    :param project_root: the path to the project
    :param ignored: a list of ignored directory names (default = ["venv", "__pycache__"])
    :param style: the style/theme used for the graph (default = "default")
    :param direction: the direction of the flowchart (TB|LR) (default = "TB")
    :return: str - the mermaid graph
    """
    from Library import MermaidStyles, project_structure

    graph = [f"flowchart {direction}\n"]

    if not ignored:
        ignored = ["venv", "__pycache__"]
    if not project_root:
        project_root = os.getcwd()
    if not style:
        style = "default"

    # if desired type == flow:  # TODO: enable for different diagram support

    ans = _structure_to_flow(project_structure(project_root, ignored), details=details, recursion_level=0)
    graph.append("".join(ans[0]))
    graph.append("".join(ans[1]))
    # elif desired type == class:
    # ...

    for elem in MermaidStyles.load_from_file().styles[style]:
        graph.append(
            f"\t{elem['def_type']} {elem['name']} fill:{elem['fill']}, "
            f"stroke:{elem['stroke']}, stroke-width:{elem['stroke-width']}px;")

    return "\n".join(graph)


def _structure_to_flow(structure: (any,), recursion_level: int = 1, details: int = 1) -> (set, list):
    """
    converts a given structure tuple as received by 'project_structure()' into a mermaid flow diagram separated in
    a diagram head and a diagram body.
    :param structure: a tuple as received by project_structure
    :param details: the amount of details that should be included 0=low...2=high (default=1)
    :param recursion_level: the current level of recursion (default=1)
    :return: (set, list) - header and body of the mermaid flow diagram
    """
    from Library import Searchable

    # separating for reliable arrow creation. Subgraph's and links must be declared separate from another
    mermaid_header = set()
    mermaid_body = []
    tabs = "\t" * recursion_level

    if recursion_level == 0:
        for elem in structure:
            ans = _structure_to_flow(elem, details=details)
            mermaid_header |= ans[0]
            mermaid_body += ans[1]

    # checking for instance first, even tho there are those nested ifs, to reduce code duplication
    elif isinstance(structure[0], Searchable):
        fn = __replace_bad_chars(structure[1])  # fn = file name
        display_name = structure[1].split(os.sep)[-1]
        mermaid_body.append(f"{tabs}subgraph {fn}[\"{display_name}\"]\n{tabs}direction TB\n")

        for sub_elem in structure[2]:
            ans = _structure_to_flow(sub_elem, recursion_level=recursion_level + 1, details=details)
            mermaid_header |= ans[0]
            mermaid_body += ans[1]

        if structure[0] == Searchable.MODULE or structure[0] == Searchable.FOLDER:
            mermaid_body.append(f"{tabs}end\n{tabs}class {fn} ModuleStyle_{recursion_level};\n")

        elif structure[0] == Searchable.TEST:
            mermaid_body.append(f"{tabs}end\n{tabs}class {fn} TestStyle;\n")

        elif structure[0] == Searchable.PYTHON or structure[0] == Searchable.INIT:
            mermaid_body.append(f"{tabs}end\n{tabs}class {fn} ClassStyle_1;\n")

    elif isinstance(structure[0], ast.Import) or isinstance(structure[0], ast.ImportFrom):
        fn = __replace_bad_chars(structure[1])
        mn = __replace_bad_chars(structure[2])  # mn = module name

        if isinstance(structure[3], ast.FunctionDef) and details >= 2:
            mermaid_header.add(f"\t{fn}{__replace_bad_chars(structure[3].name)} ==> {mn}\n")
        else:
            mermaid_header.add(f"\t{fn} ==> {mn}\n")
        # catch external libraries

    elif isinstance(structure[0], ast.FunctionDef) or isinstance(structure[0], ast.AsyncFunctionDef):
        if details == 0:
            return mermaid_header, mermaid_body

        fn = __replace_bad_chars(structure[1])
        display_name = structure[0].name
        string = f"{tabs}{fn}{__replace_bad_chars(structure[0].name)}[\"{display_name}\"]"

        if isinstance(structure[0], ast.FunctionDef):
            string += f":::FunctionStyle_1;\n"
        else:
            string += f":::FunctionStyle_2;\n"
        mermaid_body.append(string)

        for sub_elem in structure[2]:
            ans = _structure_to_flow(sub_elem, recursion_level=recursion_level + 1, details=details)
            mermaid_header |= ans[0]
            mermaid_body += ans[1]

    elif isinstance(structure[0], ast.ClassDef) or isinstance(structure[0], ast.Expr):
        pass  # TODO: change coloring for classes?

    else:
        raise NotImplementedError(f"{structure=} at {recursion_level=}")

    return mermaid_header, mermaid_body


def structure_to_documentation(structure: (any,), details: int = 1, parent: str = None) -> [(str, str)]:
    """
    converts a structure into its documentation representation
    :param structure: a tuple as received by project_structure
    :param parent: the element above the current element
    :param details: the amount of details that should be included 0=low...2=high (default=1)
    :return: [(str, str)] - (description, docstring),...
    """

    documentation = []

    if not parent:
        for sub_elem in structure:
            documentation += structure_to_documentation(sub_elem, details, parent="root")

    elif isinstance(structure[0], ast.Expr):
        documentation.append((parent, "\n".join([x.strip() for x in structure[2].split("\n")])))

    elif isinstance(structure[0], ast.Import) or isinstance(structure[0], ast.ImportFrom):
        pass  # TODO: can this be done smarter
    else:
        display_name = structure[1].split(os.sep)[-1]

        for sub_elem in structure[2]:
            documentation += structure_to_documentation(sub_elem, details, parent=f"{parent}|{display_name}")

    return documentation


def __replace_bad_chars(string: str) -> str:
    bad_chars = ["\\", "_", "."]
    for char in bad_chars:
        string = string.replace(char, "")
    return string
