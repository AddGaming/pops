"""
Handles displaying structured graphs of your python project
"""
import ast
import os
import sys


def project_structure(root_, ignored) -> (str, str, any):
    """
    Walks through the folder structure beginning at the root_ recursively and protocols the information
    :param root_: str - the folder which to analyze
    :param ignored: [str] - folder/files to ignore
    :return: (type, path, elements) - a recursive tuple that abstracts and combines information for
    representing the structure
    """
    from Library.ospp import Searchable

    result = []

    for data_container in os.listdir(root_):
        if data_container.startswith(".") or data_container in ignored:
            continue

        if os.path.isdir(f"{root_}{os.sep}{data_container}"):
            if os.path.exists(f"{root_}{os.sep}{data_container}{os.sep}__init__.py"):
                result.append((
                    Searchable.MODULE, f"{root_}{os.sep}{data_container}",
                    project_structure(f"{root_}{os.sep}{data_container}", ignored)
                ))
            else:
                result.append((
                    Searchable.FOLDER, f"{root_}{os.sep}{data_container}",
                    project_structure(f"{root_}{os.sep}{data_container}", ignored)
                ))

        if data_container.endswith(".py"):
            if data_container.startswith("__"):
                result.append((
                    Searchable.INIT,
                    f"{root_}{os.sep}{data_container}",
                    file_structure(f"{root_}{os.sep}{data_container}")
                ))
            elif data_container.find("test") != -1:
                result.append((
                    Searchable.TEST,
                    f"{root_}{os.sep}{data_container}",
                    file_structure(f"{root_}{os.sep}{data_container}")
                ))
            else:
                result.append((
                    Searchable.PYTHON,
                    f"{root_}{os.sep}{data_container}",
                    file_structure(f"{root_}{os.sep}{data_container}")
                ))

    return tuple(result)


def file_structure(file_path: str, root_node: ast = None) -> (any,):
    """
    creates a recursive tuple representing the key elements in the ast.
    Return depends on the ast element found.
    :param root_node: the ast node from which to start (default=File)
    :param file_path: str - the file which to analyze
    :return: import -> (ast, source_file, target_file, parent_ast) |
    ClassDef -> (ast, source_file, ()) |
    FunctionDef -> (ast, source_file, ())
    Expr -> (ast, source_file, docstring)
    """
    from Library import ospp

    result = []

    if not root_node:
        with open(file_path, encoding="utf-8") as file:
            code = file.read()
        root_node = ast.parse(code)

    for i, ast_elem in enumerate(root_node.body):
        match ast_elem.__class__:
            case ast.Import:
                for name in ast_elem.names:
                    if name.name not in sys.stdlib_module_names:
                        try:
                            result.append((ast_elem, file_path, ospp.find_file(name.name), root_node))
                        except RuntimeError:
                            result.append((ast_elem, file_path, name.name, root_node))
            case ast.ImportFrom:
                if ast_elem.module not in sys.stdlib_module_names:
                    try:
                        result.append((ast_elem, file_path, ospp.find_file(ast_elem.module.split(".")[-1]), root_node))
                    except RuntimeError:
                        result.append((ast_elem, file_path, ast_elem.module, root_node))
            case ast.ClassDef:
                result.append((ast_elem, file_path, file_structure(file_path, ast_elem)))
            case ast.AsyncFunctionDef:
                result.append((ast_elem, file_path, file_structure(file_path, ast_elem)))
            case ast.FunctionDef:
                result.append((ast_elem, file_path, file_structure(file_path, ast_elem)))
            case ast.Expr:
                try:
                    result.append((ast_elem, file_path, ast_elem.value.value))
                except AttributeError:
                    pass

    return tuple(result)
