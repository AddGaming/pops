"""
Outwards facing Interface of the Library Module
"""
import Library.formatting as formatting
import Library.structure_analysis as analysis
from Library.MermaidStyles import MermaidStyles
from Library.top_level_functions import *

STYLES = MermaidStyles.load_from_file()
