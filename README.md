# POps

A **P**ython Dev**Ops** tool. CLI/Library to generate documentation from your python projects.
Spend more time developing and less pleasing management.

[![pipeline status](https://gitlab.com/AddGaming/pops/badges/development/pipeline.svg)](https://gitlab.com/AddGaming/pops/-/commits/development)
[![Latest Release](https://gitlab.com/AddGaming/pops/-/badges/release.svg)](https://gitlab.com/AddGaming/pops/-/releases)

## Getting started

clone or download the repository. Then execute the main.py, and you're ready to work.

### What should I download?

| Option           | Description                                                                             |
|------------------|-----------------------------------------------------------------------------------------|
| Main Branch      | The most up to date version of the project. Treat it as an live beta.                   |
| Tags             | If you rather prefer a stable version with finished features, check out the latest tags |
| Developer Branch | No guaranties what so ever, on your own risk!                                           |

### Installation

#### POps as Library

If you want to use this project as a basis for building your own tool chain, just use the project as a library/module.
Then `import pops.Library as pops` and you have full access to the interface.
If you really care about the amount of files, you can copy and paste the `Library`
and rename it (I recommend `pops` since it will be easier to remember). Accessing the content would then be shorter and
just `import pops`

#### POps as CLI

soon...

## Usage

Use examples liberally, and show the expected output if you can.
It's helpful to have inline the smallest example of usage that you can demonstrate,
while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

If you have difficulty setting things up, feel free to hit me up on [Twitter](https://twitter.com/AddGaming2)
or Discord (Add#6938).

If you have a feature request send it in with a mail at: contact-project+addgaming-pops-r@incoming.gitlab.com

## Roadmap

- project structure as C4 :soon:
    - structure in different file types (html & png) :soon:
    - costume color theme support :white_circle:
- Mermaid Support :soon:
    - project structure flow format :b:
        - structure in different file types (md & svg & png) :soon:
        - costume color theme support :b:
    - project structure class format :white_circle:
        - structure in different file types (md & svg & png) :white_circle:
        - costume color theme support :white_circle:
    - additional structure detail level :white_circle:
- documentation :white_circle:
    - as md :soon:
    - as html :white_circle:
    - additional detail level :white_circle:
- create overview tables out of issues :white_circle:
    - GitHub support :white_circle:
        - support https login :white_circle:
        - support ssh :white_circle:
    - GitLab support :white_circle:
        - support https login :white_circle:
        - support ssh :white_circle:
- create time estimations based on past performance :white_circle:
- test coverage estimation :white_circle:
- function flow and redundancy detection :white_circle:

:white_circle: - Not started yet
:soon: - I'm working on it
:b: - Live on main branch
:white_check_mark: - in stable release

## Contributing

Feel free to fork the project. If you want to contribute to the live version of this tool, just message me via email,
and I'll add you as a developer.
As a developer you can push and change anything you want to the development branch. From there on it gets automatically
merged into the main branch if it passes all CI tests.
Which leads me to the only rule for contributing: test your changes!
For more information on testing look inside the [test folder](https://gitlab.com/AddGaming/pops/-/tree/main/tests).

## Authors and acknowledgment

@AddGaming

## License

MIT. Do what you want with this code.

## Project status

I'm developing this in my free time on the side. So don't expect to many updates. I update this to my needs.
But so can you.
