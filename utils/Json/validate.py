"""
validate files by comparing to schemas
The validation is extremely basic and just ment for catching typing mishaps if you were to modify the settings
manually.
This is not ment for any kind of production level security.
"""
import re

from utils.Json.exceptions import IncompleteSchemaDefinition, TypeMismatch, MissingAttribute, InvalidJsonFormat


def validate_iter(value: any, iter_type, element_type):
    """
    validates a iterable data type and its children
    :param value:
    :param iter_type:
    :param element_type:
    :raises IncompleteSchemaDefinition:
    :raises TypeMismatch:
    :raises InvalidJsonFormat:
    :raises MissingAttribute:
    :return:
    """
    if type(value) != iter_type:
        raise TypeMismatch(f"{value} is not of specified type {iter_type}")
    if isinstance(element_type, dict):
        for element in value:
            validate_json(element, element_type)
    elif not all((isinstance(e, element_type) for e in value)):
        raise TypeMismatch(
            f"Elements of {value} are not (all) of specified type {element_type}"
        )


def convert_string(raw: str) -> dict:
    """
    :param raw:
    :raises InvalidJsonFormat:
    :return:
    """
    if isinstance(raw, str):
        try:
            raw: dict = eval(raw)  # happy arbitrary code execution :) As said, don't deploy this outside a devs pc
        except SyntaxError:
            raise InvalidJsonFormat(f"{raw} is not a valid json")
    if not isinstance(raw, dict):
        raise InvalidJsonFormat(f"{raw} is not a valid json")
    return raw


def validate_json(raw: str | dict, schema: dict[str: dict | type]):
    """
    checks if the string representing the types adheres to the definitions inside the schema.
    If the json is valid, no error is thrown
    :param raw: the json
    :param schema: the schema
    :raises IncompleteSchemaDefinition:
    :raises TypeMismatch:
    :raises InvalidJsonFormat:
    :raises MissingAttribute:
    :return: if valid or not
    """
    raw = convert_string(raw)

    covered_keys = set()

    for raw_key, raw_val in raw.items():
        if raw_key not in schema:
            raise IncompleteSchemaDefinition(f"{raw_key} was not found in {schema}")

        elif isinstance(raw_val, dict) and isinstance(schema[raw_key], dict):
            validate_json(raw_val, schema[raw_key])

        elif isinstance(raw_val, list) and isinstance(schema[raw_key], dict):
            validate_iter(raw_val, schema[raw_key]['type'], schema[raw_key]['elements'])

        elif isinstance(schema[raw_key], tuple):
            if raw_val not in schema[raw_key]:
                raise TypeMismatch(f"{raw_val} was expected to be one of the following: {schema[raw_key]}")

        elif isinstance(schema[raw_key], re.Pattern):
            if schema[raw_key].match(raw_val) is None:
                raise TypeMismatch(f"{raw_val} doesnt match {schema[raw_key]}")

        elif not isinstance(raw_val, schema[raw_key]):
            raise TypeMismatch(f"{raw_val} is not of specified type {schema[raw_key]}")

        covered_keys.add(raw_key)

    if len(covered_keys) != len(schema):
        raise MissingAttribute(
            f"the json was missing the following attributes: {covered_keys ^ {key for key in schema} }\n"
            f"json object: {raw}\n"
            f"schema: {schema}"
        )
