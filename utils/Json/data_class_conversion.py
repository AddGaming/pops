"""
conversion of data classes to json and vice versa
"""
from dataclasses import dataclass
from enum import Enum

from utils.Json.validate import convert_string


def file_to_dataclass(cls: dataclass, data: {str: any}, eval_dict: {str: any}) -> dataclass:
    """
    coverts json dump to dataclass
    :param cls: the class to convert into
    :param data: the json dump
    :param eval_dict: if a field needs additional evaluation, provide functions that should be applied
    :return: cls
    """
    if isinstance(data, str):
        data = convert_string(data)
    class_dict = {}
    for key, val in data.items():
        if key in eval_dict:
            class_dict[key] = eval_dict[key](val)
        else:
            class_dict[key] = val
    return cls(**class_dict)


def dataclass_to_file(data: dataclass, exclude: [str] = None) -> dict:
    """
    saves the settings to the save file
    :param data: dataclass - to save to file
    :param exclude: attributes to ignore when saving
    :return: None
    """
    if exclude is None:
        exclude = tuple()

    try:
        return data.__dict__
    except AttributeError:
        dic = {}
        for name in data.__slots__:
            val = getattr(data, name)
            if name in exclude:
                continue
            elif isinstance(val, Enum):
                dic[name] = val.value
            elif type(val) not in (int, float, bool, str, dict, list, tuple):
                dic[name] = dataclass_to_file(val, exclude)
            else:
                dic[name] = val

    return dic
