"""
This package is a replacement for the jsonschema package which was a prior requirement for validating the settings.
This simple reimplementation enables the POps project to be completely dependency free
"""
from .data_class_conversion import file_to_dataclass, dataclass_to_file
from .exceptions import MissingAttribute, TypeMismatch, IncompleteSchemaDefinition, InvalidJsonFormat
from .validate import validate_json
