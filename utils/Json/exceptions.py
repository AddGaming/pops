"""
Errors while validating
"""


class IncompleteSchemaDefinition(RuntimeError):
    """
    Raised if the schema is missing fields found in the json
    """
    pass


class TypeMismatch(RuntimeError):
    """
    Raised if the type found inside the json is not matching with the type specified inside the schema
    """
    pass


class MissingAttribute(RuntimeError):
    """
    Raised if the schema has more keys than were covered by the json
    """
    pass


class InvalidJsonFormat(RuntimeError):
    """
    Raised if the provided file doesn't adhere to basic json layout
    """