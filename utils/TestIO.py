"""
Testing Utility classes
"""
from __future__ import annotations


class FakeInputGiver:
    """
    replacement for the "input" function
    every time the class gets invoked as a callable, it returns another given input
    """
    inputs: tuple
    index: int

    @classmethod
    def new(cls, *inputs) -> FakeInputGiver:
        """
        :param inputs:
        creates a new FakeInputGiver
        """
        instance = cls.__new__(cls)
        instance.inputs = inputs
        instance.index = 0
        return instance

    def __call__(self, *args):
        ans = self.inputs[self.index]
        self.index += 1
        return ans


class FakeOutputReceiver:
    """
    replacement for the "print" function
    collects all *prints* in a list
    """
    outputs: list

    @classmethod
    def new(cls) -> FakeOutputReceiver:
        """
        creates a new FakeOutputReceiver
        """
        instance = cls.__new__(cls)
        instance.outputs = []
        return instance

    def __call__(self, *args):
        self.outputs += args
