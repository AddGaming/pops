"""
Colors for the Terminals
"""
from enum import StrEnum


class Colors(StrEnum):
    """
    An Enum storing the color codes for colored Terminal output
    """
    PURPLE = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def color(string: str, col: tuple[Colors, ...]) -> str:
    """
    Colors a given string in the requested color for terminals
    :param string: text to color
    :param col: color code
    :return: colored string
    """
    for c in col:
        string = f"{c}{string}{Colors.ENDC}"
    return string


def color_doc_string(string: str) -> str:
    """
    Colors a given doc string in the requested color for terminals
    :param string: documentation
    :return: colored sting
    """
    if "example" in string.lower():
        half1, half2 = string[:string.lower().find("example")], string[string.lower().find("example"):]
        half2 = color(half2, (Colors.YELLOW,))
        string = "".join([half1, half2])

    if "`" in string:
        temp = string.split("`")
        for i in range(1, len(temp)):
            if i % 2 == 0:
                temp[i] = f"{Colors.ENDC}{temp[i]}"
            else:
                temp[i] = f"{Colors.GREEN}{temp[i]}"
        string = "".join(temp)
    return string
