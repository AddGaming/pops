"""
Class for handling user set settings management
"""
import os
from dataclasses import dataclass, field
from io import StringIO

from utils.Json import file_to_dataclass, validate_json, MissingAttribute, TypeMismatch, IncompleteSchemaDefinition, \
    dataclass_to_file, InvalidJsonFormat


@dataclass(order=True, slots=True)
class Settings:
    """
    Class for handling user set settings management
    """
    project_location: str = os.getcwd()  # the local system path
    remote_repository: str = ""  # the url
    remote_repository_type: str = ""  # GitLab | GitHub
    username: str = ""  # username used for remote auth
    # noinspection HardcodedPassword
    password: str = ""  # password used for remote auth
    theme: str = "default"  # the coloring theme used for output
    ignored_elements: [str] = field(default_factory=lambda: [])  # the list of ignored directories and files

    @staticmethod
    def _schema() -> dict:
        """
        The validation schema
        :return: Json-Validation-Schema
        """
        return {
            "project_location": str,
            "remote_repository": str,
            "remote_repository_type": str,
            "username": str,
            "password": str,
            "theme": str,
            "ignored_elements": {
                "type": list,
                "elements": str
            }
        }

    @classmethod
    def load_from_file(cls):
        """
        loads the settings from the save file
        :return: Settings
        """
        path = os.sep.join(__file__.split(os.sep)[:-1])
        try:
            with open(f'{path}{os.sep}settings.json', "r") as f:
                # no validation needed, because unaccepted input_ is caught in loads() and __init__()
                content = f.read()
        except IOError:
            print("No settings found\n\t-> booting from blank defaults...")
            c = cls()
            c.save_to_file()
            return c
        try:
            validate_json(content, cls._schema())
        except (MissingAttribute, TypeMismatch, IncompleteSchemaDefinition, InvalidJsonFormat) as e:
            print(str(e))
            print("settings.json is invalid\n\t-> instantiating a new version...")
            c = cls()
            c.save_to_file()
            return c
        return file_to_dataclass(cls, content, {})

    def save_to_file(self) -> None:
        """
        saves the settings to the save file
        :return: None
        """
        dump = str(dataclass_to_file(self))
        path = os.sep.join(__file__.split(os.sep)[:-1])
        with open(f'{path}{os.sep}settings.json', "w") as f:
            f.write(dump)

    def __show_ignore(self, o_function: callable) -> None:
        out = StringIO()
        if not self.ignored_elements:
            out.write(f"\tno ignored elements")
        folders = [e for e in self.ignored_elements if "." not in e]
        if folders:
            out.write(f"Folders:\n")
            out.write("\n".join(f"\t{e}" for e in folders))
        files = [e for e in self.ignored_elements if "." in e]
        if files:
            out.write(f"\nFiles:\n") if folders else out.write(f"Files:\n")
            out.write("\n".join(f"\t{e}" for e in files))
        o_function(out.getvalue())

    @staticmethod
    def __unify_quoted_args(args):
        if args[0][-1] != args[0][0]:
            while args[1].strip()[-1] not in {"'", '"'}:
                args[0] += " " + args.pop(1)
                if len(args) == 1:  # reached end without hitting closing quotation
                    args.append(args[0][0])
            args[0] += " " + args.pop(1)
        args[0] = args[0][1:-1]
        return args

    def __remove_ignored_elements(self, args: list[str], o_function: callable) -> None:
        while args:
            if args[0][0] in {"'", '"'}:
                args = self.__unify_quoted_args(args)

            temp = args.pop(0).strip()
            try:
                self.ignored_elements.remove(temp)
            except ValueError:
                o_function(f"{temp} is not ignored")

    def __add_ignored_element(self, args: list[str]) -> None:
        while args:
            if args[0][0] in {"'", '"'}:
                args = self.__unify_quoted_args(args)

            self.ignored_elements.append(args.pop(0).strip())

    def ignore(self, *args, o_function: callable) -> bool:
        """
        Used to edit the ignored elements list.
        Examples
        "ignore" -> list the ignored files/folders
        "ignore x y z" -> ads x,y,z to the ignored elements
        "ignore -remove x y" -> removes x,y from the ignored elements
        "ignore -r x y" -> removes x,y from the ignored elements
        :return: True (to continue parsing)
        """
        args: list[str] = list(args)
        if len(args) == 0:
            self.__show_ignore(o_function)

        elif args[0] == "-remove" or args[0] == "-r":
            self.__remove_ignored_elements(args[1:], o_function)

        else:
            self.__add_ignored_element(args)

        self.save_to_file()
        return True

    def __str__(self):
        return "\n".join(f"\t{name}: {getattr(self, name)}" for name in self.__slots__ if name != "ignored_elements")

    def set_ings(self, *args, o_function: callable = print) -> bool:
        """
        Sets the param_name to the value of param.
        If no args are passed on, it instead returns the current settings
        Examples
        "settings" -> <list of options and values>
        "settings <option name>" -> <current value of the option>
        "settings <option name> <new values>" -> reassigns the option to the new value
        "settings reset" -> resets all the settings to default values
        :param o_function: function used for output
        :param args: the user input split by spaces
        :return: True
        """
        # Yikes. this is a refactoring candidate
        output = StringIO()
        args: list[str] = list(args)

        if not args:
            output.write(str(self))

        elif args[0] == "ignored_elements":
            output.write("\tPlease use the 'ignore' keyword to set/get ignored elements")

        elif args[0] == "reset":
            self.project_location: str = os.getcwd()
            self.remote_repository: str = ""
            self.remote_repository_type: str = ""
            self.username: str = ""
            # noinspection HardcodedPassword
            self.password: str = ""
            self.theme: str = "default"
            self.ignored_elements: [str] = []
            output.write("\tThe settings were successfully reset")

        elif len(args) == 1:
            try:
                output.write(f"\t{args[0]}: {getattr(self, args[0])}")
            except AttributeError:
                output.write(f"\tAttribute '{args[0]}' doesn't exists")

        else:
            name = args[0]
            val = args[1:]
            if val[0][0] in {"'", '"'}:
                val = self.__unify_quoted_args(val)

            if len(val) == 1:
                if name == "project_location":
                    if not os.path.isdir(val[0]):
                        output.write(f"\tThe path you entered is not valid.\n\tpath: {val[0]}")
                        val[0] = self.project_location
                elif name == "remote_repository":
                    if "gitlab" in val[0] and "github" not in val[0]:
                        self.remote_repository_type = "GitLab"
                    elif "github" in val[0] and "gitlab" not in val[0]:
                        self.remote_repository_type = "GitHub"
                try:
                    getattr(self, name)
                    setattr(self, name, val[0])
                except AttributeError:
                    output.write(f"\tAttribute '{name}' doesn't exists")
            else:
                output.write(f"\tTo many arguments: {args}")

        o_function(output.getvalue())
        self.save_to_file()
        return True
