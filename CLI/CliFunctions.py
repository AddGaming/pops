"""
Wrapper around Library functions
"""
import os
from copy import deepcopy
from platform import system

from CLI.Colors import *
from Library import STYLES, graph, document


def _quit_(o_function: callable) -> bool:
    """
    shuts down the cli safely
    :return: False
    """
    from CLI import SETTINGS
    o_function("Shutting down...")
    SETTINGS.save_to_file()
    return False


def interpret_style(o_function: callable, *args) -> bool:
    """
    if executed without any parameter, returns the registered styles in the app.
    if provided with a style name, it returns more information about the style.
    :param o_function: output functions
    :param args: style name
    :return: True
    """

    if not args:
        for style in STYLES.list_styles():
            o_function(f"\t{style}")

    if len(args) == 1:
        try:
            STYLES.styles[args[0]]
        except KeyError:
            o_function("A style with this name doesn't exists")
            return True

        for def_ in deepcopy(STYLES.styles[args[0]]):
            o_function(f"{def_['name']}:")
            del def_["name"]
            for key, val in def_.items():
                o_function(f"\t{key}: {val}")
    return True


def clear(o_function: callable) -> bool:
    """
    clears the console
    :return: True
    """
    if system() == "Windows":
        os.system("cls")
    else:
        # os.system("clear")
        o_function(f"Your system ({system()}) is not supported for this operation (yet).")
    return True


def structure(o_function: callable, settings, *args) -> bool:
    """
    Analyze the structure of the program
    :return: True
    """

    if not args:
        args = [f"{settings.project_location}{os.sep}Structure.md"]

    if len(args) == 1:
        try:
            with open(args[0], "w", encoding="utf-8") as f:
                f.write(graph(path=settings.project_location, ignored=settings.ignored_elements, style=settings.theme))
            o_function(f"\tWrote mermaid to {args[0]}")
        except OSError:
            o_function(f"\tInvalid Path: {args[0]}. Did you escape it correctly?")
    return True


def documentation(o_function: callable, settings, *args) -> bool:
    """
    Generates documentation for the project based on the doc strings of the functions.

    There are 4 levels (`-l`) on which this function can operate.
    `1`. Only documentation of Modules and Files
    `2`. Documentation of Classes
    `3`. Documentation of Function and Methods
    `4`. Documentation of private functions
    If the flag is unset, the default value of `3` is used.

    There are different formats (`-f`) supported.
    1. `md` - creates the files as markdown
    2. `html` - creates the files as html
    3. `obsidian` - creates the files at the path (`-p`) that way,
                    that the folder can be used as an obsidian (obsidian.md) vault.
    If the flag is unset, the default value of `2` is used.

    The flag (-g) depicts the granularity that files should have.
    `1`. All documentation is saved in one big file. (Not recommended)
    `2`. Each file of source code becomes one file in the documentation
    `3`. Each function becomes a file in the documentation
    If the flag is unset, the default value of `2` is used.
    If this flag is used together with the levels flag (`-l`), impossible configurations result in an error

    With the (`-p`) flag a new target path for the documentation can be specified.
    If no path is given, the function defaults to `<project_location>/documentation`

    The (`-t`) flag enables you to set a costume theme for the generation.
    If not provided the theme specified in the application settings will be used.
    If you have downloaded a theme or want to create a new one, place it in the `Library/themes` folder
    of this application.

    Examples
    "documentation" -> translates to "documentation -l 3 -f html -g 2 -p <project_location/documentation>"
    "documentation -l 2" -> changes the documentation level to only document classes
    "documentation -l 1 -g 3" -> will throw an error since the documentation level is not compatible
                                 with the requested granularity of files.
    :param o_function: output function
    :param settings: settings
    :param args: [str] of user input (spit by space)
    :return: True
    """

    if not args:
        o_function(document(settings.project_location))

    return True


def parsing_loop(
    registered: dict[str: callable],
    i_function: callable,
    o_function: callable,
) -> None:
    """
    Parsing user input and executing mapper
    :param registered: the register with functions {invocation: function}
    :param i_function: the function used for input inquiry
    :param o_function: the function used for giving output
    :return: None
    """
    running = True
    while running:
        cmd_params: str = i_function(f"$: ").strip()
        cmd: str = cmd_params.split()[0].lower()
        if not cmd_params:
            continue

        if cmd in registered:
            running = registered[cmd](*cmd_params.split()[1:], o_function=o_function)
        elif cmd_params.lower() in registered:
            running = registered[cmd_params](o_function=o_function)
        else:
            o_function(f"unrecognized input: {cmd}")


def give_help(
    *args,
    register: dict[str: callable],
    encyclopedia: dict[str: str],
    o_function: callable,
    **kwargs
) -> bool:
    """
    prints the documentation of all registered functions to the screen.
    If provided a argument it returns the help for that argument instead.
    Examples
    "help ignore" -> gives help on the command `ignore`
    :param register: the register with functions {invocation: function}
    :param encyclopedia: a dictionary with explanation for words and abbreviations {str: str}
    :param o_function: the function used for giving output
    :return: True
    """
    if args:
        args = tuple(s.lower() for s in args)
        if args[0] in encyclopedia:
            o_function(f"\n{encyclopedia[args[0]]}\n")
        elif args[0] in register:
            o_function(f"\t{args[0]}:\n{register[args[0]].__doc__.split(':')[0]}")
        else:
            o_function(f"The keyword '{args[0]}' does not exist")
    else:
        output_msgs = {}
        for key, val in register.items():
            msg = val.__doc__.split(':')[0]
            if msg not in output_msgs:
                output_msgs[msg] = key
            else:
                output_msgs[msg] += f" | {key}"

        o_function("\n".join(
            f"{color(key, (Colors.BOLD, Colors.UNDERLINE, Colors.CYAN))}: {color_doc_string(msg)}"
            for msg, key in output_msgs.items()
        ))
    return True
