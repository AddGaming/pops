"""
Register with the linkage between functions and invocations
"""
from functools import partial

from CLI import SETTINGS
from .CliFunctions import structure, documentation, interpret_style, clear, give_help, _quit_


def make_register(settings=SETTINGS) -> dict[str: callable]:
    """
    :return: The input to function mapping
    """
    registered = {
        "quit": _quit_,
        # args= folder->any_str, type->md|svg|png, format=flow|class
        "structure": partial(structure, settings=settings),
        "documentation": documentation,  # args= folder->any_str, type->html|md, details=all|normal|none
        # "flow": None,  # args
        "styles": interpret_style,
        "themes": interpret_style,
        "cls": clear,
        "clear": clear,
        "ignore": settings.ignore,
        "settings": settings.set_ings,
    }
    registered["help"] = partial(give_help, register=registered, encyclopedia={})
    registered["help"].__doc__ = give_help.__doc__
    registered["structure"].__doc__ = structure.__doc__
    return registered
