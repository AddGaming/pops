"""
The CLI module.
Maps functionality from the Library to executable functions for the user
"""
from CLI.CliFunctions import parsing_loop
from CLI.Settings import Settings

SETTINGS = Settings.load_from_file()

from CLI.FunctionRegister import make_register

